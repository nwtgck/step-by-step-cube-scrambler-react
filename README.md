# Step-by-Step Rubik's Cube Scrambler

![v1.1.0 demo image](./demo-images/v1.1.0.gif)

## Page

https://nwtgck.github.io/step-by-step-cube-scrambler-react/

## Main features

* React
* TypeScript

## How to run the server

```bash
$ cd <this repo>
$ npm install
$ npm run build
```

## How to deploy the server to `gh-pages`

```bash
$ cd <this repo>
$ npm install
$ npm run deploy
```